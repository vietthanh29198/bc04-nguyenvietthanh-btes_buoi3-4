var renderDscv = function (listCv) {
  var contentHTML = "";

  listCv.forEach((cv) => {
    var contentLi = `<li>${cv.name}
    <span class="buttons">
    <button onclick="suaCv(${cv.id})">
        <i class="edit fa fa-edit"></i>
    </button> 
    <button onclick="xoaCv(${cv.id})">
        <i class="remove fa-solid fa-trash-can"></i>
    </button> 
    <button onclick="hoanThanhCv(${cv.id})">
        <i class="complete fa fa-check-circle"></i>
    </button>
    </span>
  </li>`;
    contentHTML += contentLi;
  });
  document.getElementById("todo").innerHTML = contentHTML;
};

var renderCompleteTasks = function (listCv) {
  var contentHTML = "";

  listCv.forEach((cv) => {
    var contentLi = `<li>${cv.name}
    <span class="buttons">
    <button>
        <i class="complete fa fa-check-circle"></i>  
    </button>
    </span>
    </li>`;
    contentHTML += contentLi;
  });
  document.getElementById("completed").innerHTML = contentHTML;
};

function openLoading() {
  document.getElementById("loading").style.display = "flex";
}
function closeLoading() {
  document.getElementById("loading").style.display = "none";
}
